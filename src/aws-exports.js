// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
const awsmobile = {
    'aws_app_analytics': 'enable',
    'aws_cognito_identity_pool_id': 'us-east-1:22d9c3da-f25b-4b72-baab-57af3a7daffa',
    'aws_cognito_region': 'us-east-1',
    'aws_content_delivery': 'enable',
    'aws_content_delivery_bucket': 'hiveresearch-hosting-mobilehub-1323696906',
    'aws_content_delivery_bucket_region': 'us-east-1',
    'aws_content_delivery_cloudfront': 'enable',
    'aws_content_delivery_cloudfront_domain': 'd63qrllhem9xl.cloudfront.net',
    'aws_mobile_analytics_app_id': '1918f19b78e046f9bc2325fb613d82d4',
    'aws_mobile_analytics_app_region': 'us-east-1',
    'aws_project_id': 'c733b85a-1384-40fe-a365-2b22d180e8d0',
    'aws_project_name': 'hive-research',
    'aws_project_region': 'us-east-1',
    'aws_resource_name_prefix': 'hiveresearch-mobilehub-1323696906',
}

export default awsmobile;
