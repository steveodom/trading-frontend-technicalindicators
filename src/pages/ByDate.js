import React, { Component } from 'react';
import Layout from './layout';

import { Link } from 'react-router-dom';
import {Button} from 'rmwc/Button';
import { Grid, GridCell } from 'rmwc/Grid';
import IntroBar from './../components/primitives/IntroBar';
import Dropdown from './../components/primitives/dropdown';

import {fetchRecentDates} from './../queries/byDate';
import DateDetails from '../containers/DateDetails';
import Storage from './../utils/storage';

import {hydratedCounts} from './../queries/hydratedCounts';

import { Analytics } from 'aws-amplify';

function MenuButton(props) {
  return (
    <Link to={{ pathname: '/' }}>
      <Button 
        unelevated 
        className="layout--right-action"
        theme={['secondary-bg']}
        style={{borderColor: '#E91E63'}}
      >
        View Historical
      </Button>
    </Link>
  )
}


export default class ByDate extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      recentDates: [],
      historicalCounts: [],
      indicatorNames: [],
      targetDate: ""
    };
    this.storage = new Storage("session");
    this.handleChangeDate = this.handleChangeDate.bind(this)
  }

  handleChangeDate(newDate) {
    this.setState({targetDate: newDate})
  }

  async fetchNamesAndCounts() {
    const {hydrated, indicatorNames} = await hydratedCounts();
    this.setState({historicalCounts: hydrated, indicatorLoading: false, indicatorNames: indicatorNames});
    this.storage.save("indicatorNames", indicatorNames);
    this.storage.save("counts", hydrated);
  }

  addNamesAndCounts() {
    const indicatorNames = this.storage.get("indicatorNames");
    const items = this.storage.get("counts");
    
    if (indicatorNames && items) {
      this.setState({historicalCounts: items, indicatorLoading: false, indicatorNames: indicatorNames});
    } else {
      this.fetchNamesAndCounts();
    }
  }

  componentDidMount() {
    Analytics.record('bydate-page-view');
    const recentDates = this.storage.get("recentDates");

    if (recentDates) {
      this.setState({recentDates: recentDates, targetDate: recentDates[0]});
    } else {
      fetchRecentDates().then((res) => {
        this.setState({recentDates: res, targetDate: res[0]})
        this.storage.save("recentDates", res);
      });
    }

    this.addNamesAndCounts()
  }

  render() {
    const {recentDates, targetDate, historicalCounts} = this.state;
  
    return (
      <Layout 
        title=""
        RightButton={<MenuButton className="layout--right-action"/>}  
      >
        <Grid>
          <GridCell span="1"></GridCell>
          <GridCell span="10">
            <Grid>
              <GridCell span="9">
                <IntroBar 
                  title={`Indicator Combinations Hit on ${targetDate}`}
                >
                    Everyday we track which technical and momentum indicators are hit (and combinations thereof) for a <Link to={{pathname: "/datasources"}}>group of tickers</Link>. We then <Link to={{pathname: "/"}}>count them up</Link> based on whether the stock proceeds to go up or down over the next 5 days.
                </IntroBar>
              </GridCell>
              <GridCell span="3" style={{marginTop: "-3px"}}>
                <Dropdown 
                  title="Change Date:"
                  optionArray={recentDates}
                  handleDropdownChange={this.handleChangeDate}
                  currentValue={targetDate}
                />
              </GridCell>
            </Grid>
            {targetDate && 
              <DateDetails 
                date_human={targetDate} 
                historicalCounts={historicalCounts} 
                recentDates={recentDates}
                handleChangeDate={this.handleChangeDate}
              />
            }
          </GridCell>
        </Grid>
      </Layout>

    )
  }
}