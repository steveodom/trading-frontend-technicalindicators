import React, { Component} from 'react';

import {Button} from 'rmwc/Button';
import { Toolbar, ToolbarRow, ToolbarSection, ToolbarTitle } from 'rmwc/Toolbar';

import MainMenu from '../components/layout/main-menu';
import {ContactButton} from './../components/primitives/buttons';

import '../../node_modules/material-components-web/dist/material-components-web.min.css';
import './layout.css';
import logo from './../images/icon.png';

import { Analytics } from 'aws-amplify';

export default class Layout extends Component {  
  constructor(props) {
    super(props);
    this.state = {drawerOpen: false};
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
    this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
  }

  handleOpenDrawer() {
    this.setState({drawerOpen: true});
    Analytics.record('right-menu-open');
  }

  handleCloseDrawer() {
    this.setState({drawerOpen: false});
    Analytics.record('right-menu-close');
  }

  render() {
    const {title, children, menuIcon, RightButton} = this.props;
    const {drawerOpen} = this.state;
    
    return (
      <div className='layout'>
      
        <Toolbar fixed>
          <ToolbarRow>
            <ToolbarSection alignStart theme={['text-primary-on-background']}>
              {menuIcon}
              <ToolbarTitle style={{paddingTop: '6px'}}>
                {!menuIcon && 
                  <a href="http://www.hiveresearch.com">
                    <img className="logo" src={logo} alt="Hive Research" /> 
                  </a>
                }
                {title}
              </ToolbarTitle>
            </ToolbarSection>
            <ToolbarSection alignEnd>
              {!RightButton && <ContactButton />}
              {RightButton}
            </ToolbarSection>
          </ToolbarRow>
        </Toolbar>
        <MainMenu open={drawerOpen} handleCloseDrawer={this.handleCloseDrawer}/>
        <div className="body--content">
          { children }
        </div>
        <div className="layout--bottom-right">
            <Button 
              className="layout--right-action"
              dense={true} 
              stroked 
              style={{color: "#aaa", borderColor: "#aaa"}}
              onClick={this.handleOpenDrawer}
            >
              Feedback
            </Button>
        </div>
      </div>
    )
  }
}