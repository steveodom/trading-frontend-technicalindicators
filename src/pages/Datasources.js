import React, { Component } from 'react';
import Layout from './layout';
import IntroBar from './../components/primitives/IntroBar';

import Loader from '../components/primitives/loader';

import { List, ListItem, ListItemText } from 'rmwc/List';
import { Grid, GridCell } from 'rmwc/Grid';

import { Analytics } from 'aws-amplify';
import {fetchTickers} from './../queries/fetchTickers';


export default class Datasources extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      tickers: [],
      loading: true
    };
  }

  componentDidMount() {
    Analytics.record('detail-page-view');
    fetchTickers().then((res) => {
      this.setState({tickers: res, loading: false});
    });
  }

  render() {
    const {loading, tickers} = this.state;
    const sortedTickers = tickers.sort();
    return (
      <Layout title="">
        <Grid>
          <GridCell span="1"></GridCell>
          <GridCell span="11"> 
            <IntroBar title="Data Sources">
                These are the list of tickers used in the data analysis. More are added regularly. For daily data, the we grab daily quotes for the past three years.
            </IntroBar>
          </GridCell>
          <GridCell span="1"></GridCell>
          <GridCell span="4"> 
            {loading && <Loader />}
            {!loading && 
              <List className="list-overview__list">
                {sortedTickers.map( (ticker) => {
                  return (
                    <ListItem className="list-overview__list-item">
                      <ListItemText className="wrap list-overview__middle-content">
                          {ticker}
                      </ListItemText>
                    </ListItem>
                  )
                })}
              </List>
            }
          </GridCell>
        </Grid>
      </Layout> 
    )
    
  }
}