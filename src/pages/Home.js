import React, { Component } from 'react';
import Layout from './layout';

import isempty from 'lodash.isempty';

import { Link } from 'react-router-dom';
import {Button} from 'rmwc/Button';
import {Icon} from 'rmwc/Icon';

import DetailsDialog from '../components/details-dialog';
import ListOverview from '../components/ListOverview';
import ActiveTerms from '../components/ActiveTerms';
import IntroBar from './../components/primitives/IntroBar';
import Dropdown, {DropdownTextWithIcon} from './../components/primitives/dropdown';

import { Analytics } from 'aws-amplify';
import {hydratedCounts} from './../queries/hydratedCounts';

import {filterHandler} from './../utils/filtering';
import {directionButtonText} from './../utils/formatter';

import {DailyButton} from './../components/primitives/buttons';
import { Grid, GridCell } from 'rmwc/Grid';
import "./../components/primitives/select.css";

import Storage from './../utils/storage';
import { Typography } from 'rmwc/Typography';

import {JSONLD_HOMEPAGE, JsonLd} from './../utils/json-ld';

export default class Home extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      dialogOpen: false,
      terms: [],
      items: [],
      indicatorNames: [],
      sortKey: "ratio",
      sortLabel: "Sort by: Ratio",
      direction: 'both',
      indicatorLoading: true,
    };

    this.storage = new Storage("session");
    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleClickOnFilter = this.handleClickOnFilter.bind(this);
    this.handleClickOnDirection = this.handleClickOnDirection.bind(this);
    this.handleClickOnSort = this.handleClickOnSort.bind(this);
  }

  handleOpenDialog() {
    this.setState({dialogOpen: true});
    Analytics.record('dialog-menu-open');
  }

  async fetchNamesAndCounts() {
    const {hydrated, indicatorNames} = await hydratedCounts();
    this.setState({items: hydrated, indicatorLoading: false, indicatorNames: indicatorNames});
    this.storage.save("indicatorNames", indicatorNames);
    this.storage.save("counts", hydrated);
  }

  componentDidMount() {
    Analytics.record('home-page-view');
    const indicatorNames = this.storage.get("indicatorNames");
    const items = this.storage.get("counts");
    
    if (indicatorNames && items) {
      this.setState({items: items, indicatorLoading: false, indicatorNames: indicatorNames});
    } else {
      this.fetchNamesAndCounts()
    }
    
  }

  sortResults(results) {
    const {sortKey} = this.state;
    const sorted = results.sort( (a,b) => (b[sortKey] > a[sortKey]));
    return sorted;
  }

  filter() {
    return this.state.items.filter( (item) => {
      const {direction} = this.state;
      let indicatorPasses = [];
      let pass;
      this.state.terms.forEach( (filter) => {
          pass = filter.klass !== 'direction' && item.id.includes(filter.id);
          indicatorPasses.push(pass);
      });

      if (isempty(this.state.terms) && item.count < 100) {
        indicatorPasses = [false];
      }
      
      const passesDirectionCondition = direction === 'both' ? true : direction === item.direction;
      const passesIndicatorCondition = indicatorPasses.includes(true) || isempty(indicatorPasses);

      return passesDirectionCondition && passesIndicatorCondition;
    });
  }

  handleClickOnFilter(filter) {
    const modified = filterHandler(filter, this.state.terms)
    this.setState({terms: modified});
  }

  handleClickOnSort(sortKey, sortLabel) {
    this.setState({sortKey, sortLabel});
  }

  handleCloseDialog() {
    this.setState({dialogOpen: false});
  }

  handleClickOnDirection(targetDirection) {
    const {direction} = this.state;
    let modified = 'both';
    if (direction === 'both') {
      modified = targetDirection;
    } else {
      if (targetDirection !== direction) {
        modified = targetDirection;
      }
    }
    this.setState({direction: modified});
  }

  render() {
    const {
      dialogOpen, 
      terms, 
      direction, 
      sortLabel, 
      indicatorNames, 
      indicatorLoading
    } = this.state;
    const listable = this.filter();
    const sortedResults = this.sortResults(listable);
    const directionLabel = direction === 'both' ? 'Both Directions' : `Stocks ${directionButtonText(direction)}`;
    
    return (
      <Layout 
        title=""
        RightButton={<DailyButton />}
      >   <Grid style={{marginBottom: "1rem"}}>
            <GridCell span="1"></GridCell>
            <GridCell span="8">
              <IntroBar 
                title="Frequency of TA Indicators by Nearterm Market Growth"
              >
                  Everyday we track which technical and momentum indicators are hit (and combinations thereof) for a <Link to={{pathname: "/datasources"}}>group of tickers</Link>. We then <Link to={{pathname: "/"}}>count them up</Link> based on whether the stock proceeds to go up or down over the next 5 days. This is the summary of those counts.
              </IntroBar>
            </GridCell>
            <GridCell span="2" style={{marginTop: "-12px"}}>
              <Typography use="caption">Table Options</Typography>
              <Grid style={{padding: "0", margin: "0"}}>
                <GridCell span="12">
                  <Dropdown 
                    optionHash={[{label: "Frequency", val: "count", val2: "Frequency"}, {label: "Up/Down Ratio", val: "ratio", val2: "Up/Down Ratio"}]}
                    handleDropdownChange={this.handleClickOnSort}
                    currentValue={<DropdownTextWithIcon text={sortLabel} icon="sort"/>}
                    menuStyle={{marginLeft: "-24px"}}
                  /> 
                </GridCell>

                <GridCell span="12" style={{marginTop: "-24px"}}>
                  <Dropdown 
                    optionHash={[{label: "Stocks go up", val: "up"}, {label: "Stocks go down", val: "down"}]}
                    handleDropdownChange={this.handleClickOnDirection}
                    currentValue={<DropdownTextWithIcon text={directionLabel} icon="done"/>}
                    menuStyle={{marginLeft: "-24px"}}
                  />
                </GridCell>
                
                <GridCell span="12" style={{marginTop: "-24px", marginLeft: "-24px"}}>
                  <Button
                    onClick={this.handleOpenDialog}
                  >
                    <Icon>filter_list</Icon> More Filters
                  </Button>
                </GridCell>
              </Grid>
            </GridCell>
          </Grid>
          <DetailsDialog 
            open={dialogOpen}
            handleClickOnFilter={this.handleClickOnFilter}
            handleClose={this.handleCloseDialog}
            activeFilters={terms}
            allIndicators={indicatorNames}
            direction={[]}
          />
          
          <Grid style={{paddingTop: "0"}}>
            
            {!isempty(terms) && 
              <GridCell span="12">
                <ActiveTerms 
                  terms={terms}
                  handleOpenDialog={this.handleOpenDialog}
                  handleClickOnFilter={this.handleClickOnFilter}
                  showAddFiltersWhenEmpty={false}
                  specialClass="no-bottom-pad"
                />
              </GridCell>
            }
            
            <GridCell span="1"></GridCell>
            <GridCell span="7">
              <ListOverview 
                results={sortedResults} 
                names={indicatorNames} 
                handleOpenDrawer={this.handleOpenDialog}
                handleFilterClick={this.handleClickOnFilter}
                loading={indicatorLoading}
              />
            </GridCell>
          </Grid>
          <JsonLd data={JSONLD_HOMEPAGE} />
      </Layout>
    )
  }
}