import React, { Component } from 'react';

import isempty from 'lodash.isempty';

import Layout from './layout';

import DetailsDialog from '../components/details-dialog';
import GridDetails from '../components/GridDetails';
import IntroBar from './../components/primitives/IntroBar';
import Dropdown, {DropdownTextWithIcon} from './../components/primitives/dropdown';
import { Typography } from 'rmwc/Typography';

import {Button} from 'rmwc/Button';
import {Icon} from 'rmwc/Icon';

import { Grid, GridCell } from 'rmwc/Grid';
import "./../components/primitives/select.css";

import ActiveTerms from '../components/ActiveTerms';
import Empty from './../components/primitives/empty';
import Loader from '../components/primitives/loader';

import { ToolbarMenuIcon } from 'rmwc/Toolbar';

import { Link } from 'react-router-dom';

import { Analytics } from 'aws-amplify';

import { fetchAllNames } from './../queries/listAllIndicatorNames';

import { filterHandler, isValidDetailFiltering } from './../utils/filtering';
import { buildTermsFromIndex, buildIndexFromTerms } from './../utils/formatter';

function BackButton(props) {
  return (
    <Link to={{ pathname: '/' }}>
      <ToolbarMenuIcon>arrow_back</ToolbarMenuIcon>
    </Link>
  )
}

export default class Details extends Component { 
  constructor(props) {
    super(props);
    const search = props.location.search; // could be '?foo=bar'
    const params = new URLSearchParams(search);
    
    this.state = {
      dialogOpen: false,
      indicatorNames: [],
      terms: [],
      index: params.get('index'),
      direction: ["up", "down"],
      sortKey: "date",
      sortLabel: "Date",
      chartKey: "price",
      chartLabel: "Chart: Price",
      loading: true
    };
    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleClickOnFilter = this.handleClickOnFilter.bind(this);
    this.handleClickOnDirection = this.handleClickOnDirection.bind(this);
    this.handleClickOnSort = this.handleClickOnSort.bind(this);
    this.handleClickOnChart = this.handleClickOnChart.bind(this);
  }

  handleClickOnSort(sortKey, sortLabel) {
    this.setState({sortKey, sortLabel});
  }

  handleClickOnChart(chartKey, chartLabel) {
    this.setState({chartKey, chartLabel});
  }

  handleOpenDialog() {
    this.setState({dialogOpen: true});
  }

  handleCloseDialog() {
    this.setState({dialogOpen: false});
  }

  handleClickOnFilter(filter) {
    const modified = filterHandler(filter, this.state.terms);
    const index = buildIndexFromTerms(modified);
  
    this.setState({
      terms: modified, 
      index
    });
  }

  handleClickOnDirection(clickedDirection) {
    const {direction} = this.state;
    let modified = []
    if (direction.includes(clickedDirection)) {
      modified = direction.filter( d => d !== clickedDirection);
      if (isempty(modified)) {
        modified = ["up", "down"]
      }
    } else {
      modified = [...direction, clickedDirection]
    }
    this.setState({direction: modified});
  }

  componentDidMount() {
    Analytics.record('detail-page-view');
    fetchAllNames().then((res) => {
      const terms = buildTermsFromIndex(this.state.index, res)
      this.setState({indicatorNames: res, terms: terms, loading: false});
    });
  }

  render() {
    const {
      indicatorNames, 
      dialogOpen, 
      terms, 
      index,
      chartKey, 
      chartLabel, 
      direction, 
      sortLabel,
      loading
    } = this.state;
    const isValidQuery = isValidDetailFiltering(terms, direction);
    if (loading) {
      return (<Loader />)
    }
    return (
      <Layout 
        title="Back to Summary" 
        menuIcon={<BackButton />}
        RightButton={<span></span>}
      > 
        <Grid style={{marginBottom: "1rem"}}>
            <GridCell span="1"></GridCell>
            <GridCell span="8">
              <IntroBar 
                title="Instances where the below indicators where hit"
              >
                  Each card below shows when the below indicator combination were hit, along with data on how much the ticker went up or down over the proceeding period.
              </IntroBar>
            </GridCell>
            <GridCell span="2" style={{marginTop: "-12px"}}>
              <Typography use="caption">Table Options</Typography>
              <Grid style={{padding: "0", margin: "0"}}>
                <GridCell span="12">
                  <Dropdown 
                    optionHash={[{label: "Price Chart", val: "price", val2: "Price Chart"}, {label: "MACD", val: "macd", val2: "MACD"}]}
                    handleDropdownChange={this.handleClickOnChart}
                    currentValue={<DropdownTextWithIcon text={chartLabel} icon="insert_chart"/>}
                    menuStyle={{marginLeft: "-24px"}}
                  /> 
                </GridCell>

                <GridCell span="12" style={{marginTop: "-24px"}}>
                  <Dropdown 
                    optionHash={[
                      {label: "Date", val: "date_human", val2: "Date"}, 
                      {label: "Ticker", val: "ticker", val2: "Ticker"},
                      {label: "Growth", val: "future_growth", val2: "Growth"},
                    ]}
                    handleDropdownChange={this.handleClickOnSort}
                    currentValue={<DropdownTextWithIcon text={sortLabel} icon="sort"/>}
                    menuStyle={{marginLeft: "-24px"}}
                  />
                </GridCell>
                
                <GridCell span="12" style={{marginTop: "-24px", marginLeft: "-24px"}}>
                  <Button
                    onClick={this.handleOpenDialog}
                  >
                    <Icon>filter_list</Icon> Edit Indicators
                  </Button>
                </GridCell>
              </Grid>
            </GridCell>
          </Grid> 
          <DetailsDialog 
            open={dialogOpen}
            handleClickOnFilter={this.handleClickOnFilter}
            handleClose={this.handleCloseDialog}
            activeFilters={terms}
            allIndicators={indicatorNames}
            page="details"
          />
          
          <Grid style={{paddingTop: "0"}}>
            {!isempty(terms) && 
              <GridCell span="12">
                <ActiveTerms 
                  terms={terms}
                  handleOpenDialog={this.handleOpenDialog}
                  handleClickOnFilter={this.handleClickOnFilter}
                  page="details"
                />
              </GridCell>
            }  
          </Grid>

          {isValidQuery && direction.length === 1 &&
            <GridDetails 
              index={index}
              handleClickOnDirection={this.handleClickOnDirection}
              sortKey={this.state.sortKey}
              isValidQuery={isValidQuery}
              chartKey={chartKey}
              direction={direction[0]}
              isOnlyOneDirection={true}
            />
          }

          {isValidQuery && direction.length === 2 &&
            <Grid>
              <GridCell span="6">
                <GridDetails 
                  index={index}
                  handleClickOnDirection={this.handleClickOnDirection}
                  sortKey={this.state.sortKey}
                  isValidQuery={isValidQuery}
                  chartKey={chartKey}
                  direction="up"
              />
              </GridCell>
              <GridCell span="6">
                <GridDetails
                    index={index} 
                    handleClickOnDirection={this.handleClickOnDirection}
                    sortKey={this.state.sortKey}
                    isValidQuery={isValidQuery}
                    chartKey={chartKey}
                    direction="down"
                />
              </GridCell>
            </Grid>
          }

          { !isValidQuery &&
            <Empty>At least one indicator filter <a style={{textDecoration: "underline"}} onClick={this.handleOpenDialog}>must be added</a></Empty>
          }

      </Layout>
    )
  }
}
