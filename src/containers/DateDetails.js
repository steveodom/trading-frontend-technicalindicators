import React, { Component} from 'react';

import { graphql, compose } from 'react-apollo';
import FetchInstancesByDate from './../queries/byDate';

import { Grid, GridCell } from 'rmwc/Grid';

import DateDetailsSection from './../components/DateDetailsSection';
import { groupAndHydrateDaily} from './../utils/formatter';

import './../components/list-overview.css';


class DateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recentDates: [],
      showUp: false,
      showDown: false
    };

    this.toggleDetailView = this.toggleDetailView.bind(this);
  }

  toggleDetailView(direction, keepOpen = false) {
    if (direction === "up") {
      if (keepOpen) {
        this.setState({showUp: true});
      } else {
        this.setState({showUp: !this.state.showUp});
      }
    } else {
      if (keepOpen) {
        this.setState({showDown: true});
      } else {
        this.setState({showDown: !this.state.showDown});
      }
    }
  }

  render() {
    const {
      date_human, 
      loading, 
      historicalCounts, 
      results
    } = this.props;
    const {showUp, showDown} = this.state;
    const {
      highUpRatios, 
      highDownRatios,
      upTickers,
      downTickers,
      lookAheadDate
    } = groupAndHydrateDaily(historicalCounts, results, date_human);
    
    return (
      <div>
        <Grid style={{paddingTop: "0"}}>
          <GridCell span="12">
            <DateDetailsSection 
              items={highUpRatios} 
              loading={loading} 
              direction="up"
              tickers={upTickers}
              showDetails={showUp}
              toggleDetailView={this.toggleDetailView}
              targetDate={lookAheadDate}
              dateHuman={date_human}
            />
          </GridCell>
          <GridCell span="12" style={{marginTop: "2rem"}}>
            <DateDetailsSection 
              items={highDownRatios} 
              loading={loading} 
              direction="down"
              tickers={downTickers}
              showDetails={showDown}
              toggleDetailView={this.toggleDetailView}
              targetDate={lookAheadDate}
              dateHuman={date_human}
            />
          </GridCell>
        </Grid>
        
      </div>
    )
  }
}


export default compose(
  graphql(
    FetchInstancesByDate,
      {
        options: (ownProps) => ({
          fetchPolicy: 'cache-and-network',
          variables: { 
            date_human: ownProps.date_human
          } 
        }),
        props: ({ 
          data: { 
            fetchInstancesByDate = {data: [], len: 0},
            loading = true
          } 
        }) => ({
            results: [...fetchInstancesByDate.data],
            len: fetchInstancesByDate.len,
            LastEvaluatedKey: fetchInstancesByDate.LastEvaluatedKey,
            loading: loading
        })
      }
  )
)(DateDetails);