import {fetchAllCounts} from './listAllCounts';
import {fetchAllNames} from './listAllIndicatorNames';

export const hydratedCounts = (params) =>  {
  let names = []
  let namesById = {}
  return fetchAllNames().then((res) => {
    names = res;
    names.forEach( obj => namesById[obj.id] = obj);
    return fetchAllCounts();
  }).then( (res) => {
    const hydrated = res.map( (item) => {
      const ids = item.id.split("-");
      const names = ids.map( id => namesById[id]);
      return Object.assign({}, item, {
        ratio: item.opposite_count ? Math.round(item.count / item.opposite_count * 100) / 100 : null,
        names
      })
    });
    return {hydrated, indicatorNames: names};
  });  
};
