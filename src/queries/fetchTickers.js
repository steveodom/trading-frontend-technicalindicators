import request from 'request-promise';

export const fetchTickers = () =>  {
  return request({
    uri: 'https://s3.amazonaws.com/trading-logs-trades/technicalcounter/tickers.json'
  }).then( (res) => {
    const js = JSON.parse(res);
    return js.tickers;
  }).catch( () => {
    return [];
  });    
};