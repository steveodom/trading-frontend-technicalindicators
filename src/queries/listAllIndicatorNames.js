import gql from 'graphql-tag';
import request from 'request-promise';
import Promise from 'bluebird';

export const fetchAllNames = () =>  {
  return new Promise( (resolve, reject) => {
    return request({
      uri: 'https://s3.amazonaws.com/trading-logs-trades/technicalcounter/names.json'
    }).then( (res) => {
      const js = JSON.parse(res);
      return resolve(js);
    }).catch( () => {
      return reject([]);
    });
  });    
};

export default gql(`
query {
  allIndicatorNames(limit: 1500) {
    id
    name
    klass
  }
}`);