import gql from 'graphql-tag';

// : "[{\"id\":\"direction\",\"val\":\"up\",\"condition\":\"eq\",\"klass\":\"direction\"},{\"id\":\"ticker\",\"val\":\"FB\",\"condition\":\"eq\",\"klass\":\"indicator\"}]"
export default gql(`
  query FetchFromTechnicalInstancesLambda($id: String!, $direction: String, $projection_expression: String, $limit: Int) {
    fetchFromTechnicalInstances(
      direction: $direction,
      index: $id,
      projection_expression: $projection_expression,
      limit: $limit
    ) {
      data {
        date_number, 
        ticker, 
        date_human,
        direction,
        lookahead_period,
        future_target_date,
        future_growth
      }
    }   
  } 
`);