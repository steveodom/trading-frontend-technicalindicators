import gql from 'graphql-tag';
import request from 'request-promise';

export default gql(`
  query FetchInstancesByDate($date_human: String!) {
    fetchInstancesByDate(
      date_human: $date_human,
      period: "daily",
      projection_expression: "id, indicators, date_number, ticker, date_human, direction, lookahead_period, future_target_date, future_growth",
      queryType: "byDate"
    ) {
      data {
        indicators, 
        date_number, 
        ticker, 
        date_human,
        direction,
        lookahead_period,
        future_target_date,
        future_growth
      }
    }
  } 
`);

export const fetchRecentDates = () =>  {
  return request({
    uri: 'https://s3.amazonaws.com/trading-logs-trades/technicalcounter/recent_dates.json'
  }).then( (res) => {
    const js = JSON.parse(res).sort().reverse();
    return js;
  }).catch( () => {
    return [];
  });    
};
