import request from 'request-promise';

export const fetchAllCounts = () =>  {
  return request({
    uri: 'https://s3.amazonaws.com/trading-logs-trades/technicalcounter/daily.json'
  }).then( (res) => {
    const js = JSON.parse(res);
    const ary = Object.values(js);
    
    const filtered = ary.filter( (item) => {
      return (
        item.direction === 'up' || item.direction === 'down') &&
        item.count > 0
    });
    
    return filtered;
  }).catch( () => {
    return [];
  });    
};
