import React from 'react';
import isempty from 'lodash.isempty';

import {Button} from 'rmwc/Button';
import {Icon} from 'rmwc/Icon';
import { FilterButton } from "./../components/primitives/buttons";

import './ActiveTerms.css';


export default (props) => {
  const indicatorFilters = props.terms.filter( (term) => (term.klass !== "direction"));
  let showAddFiltersWhenEmpty = true;
  if (props.showAddFiltersWhenEmpty === false) {
    showAddFiltersWhenEmpty = false;
  }
  return (
    <ul className={`active-terms ${props.specialClass}`}>
      {showAddFiltersWhenEmpty && isempty(indicatorFilters) &&
        <Button 
          theme={['text-hint-on-background']}
          onClick={props.handleOpenDialog}
        >
          Add Filters
        </Button>
      }
      {indicatorFilters.map( (term, i) => {
        return (
          <li key={`indicator-${i}`}>
              <FilterButton 
                key={i}
                name={term.name}
                filter={term}
                handleClickOnFilter={props.handleClickOnFilter}
              />
          </li>
        )
      })}

      

      {!isempty(indicatorFilters) &&
        <li>
          <Button 
            theme={['text-hint-on-background']}
            style={{minWidth: '32px', padding: '0 0.5rem'}}  
            onClick={props.handleOpenDialog}
          >
            <Icon style={{verticalAlign: 'middle'}}>add</Icon>
          </Button>
        </li>
      }
    </ul>
  )
}

