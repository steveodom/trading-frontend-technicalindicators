import React, { Component} from 'react';
import isempty from 'lodash.isempty';
import moment from 'moment';

import { GridList, GridTile, GridTilePrimary, GridTilePrimaryContent, GridTileSecondary, GridTileTitle, GridTileTitleSupportText } from 'rmwc/GridList';
import { Grid, GridCell } from 'rmwc/Grid';
import {Icon} from 'rmwc/Icon';
import {Button} from 'rmwc/Button';

import {directionColor, directionIcon, directionButtonText, calcOppositeDirection} from './../utils/formatter';
import { graphql, compose } from 'react-apollo';

import ListIndicators from './../queries/listIndicators';
import Loader from './primitives/loader';
import Empty from './primitives/empty';

import './GridDetails.css';

function Item(props) {
  const url = `https://s3.amazonaws.com/trading-logs-trades/charts/indicators/${props.ticker}/${props.date_human}_${props.chartKey}.png`;
  const icon = directionIcon(props.direction);
  const roundedChange = Math.round(props.future_growth * 100) / 100;
  const futureDate = moment(props.future_target_date, "YYYY-MM-DD").format("MM-DD")

  return (
    <GridTile>
				<GridTilePrimary>
					<GridTilePrimaryContent>
						<img 
              src={url} 
              alt={`${props.ticker}-chart`} 
              style={{top: "57px", height: "200px"}}
            />
					</GridTilePrimaryContent>
				</GridTilePrimary>
				<GridTileSecondary theme={['text-primary-on-dark']} style={{backgroundColor: directionColor(props.direction)}}>
          <GridTileTitle>
            {props.ticker} <small>ending {props.date_human}</small>
          </GridTileTitle>
					<GridTileTitleSupportText>
            By {futureDate} <Icon>{icon}</Icon> {roundedChange}%
          </GridTileTitleSupportText>
				</GridTileSecondary>
			</GridTile>
  )
}

class GridDetails extends Component {
  sortResults() {
    const {sortKey, results, direction} = this.props;
    
    let sorted = [];
    if (direction === 'down' && sortKey==='future_growth') {
      sorted = results.sort( (a,b) => (b[sortKey] < a[sortKey]));
    } else {
      sorted = results.sort( (a,b) => (b[sortKey] > a[sortKey]));
    }
      
    return sorted;
  }
  render() {
    const {
      loading, 
      chartKey, 
      direction,
      handleClickOnDirection,
      isOnlyOneDirection
    } = this.props;
    
    const sortedResults = this.sortResults()
   
    const headerColWidth = isOnlyOneDirection ? 6 : 12;
    let opposite;

    if (isOnlyOneDirection) {
      opposite = calcOppositeDirection(direction);
    }

    return (
      <div className="content--full" style={{marginTop: "-3rem"}}>
        <div className="gridlist--titlebar">
          <Grid style={{padding: "0"}}>
            <GridCell span={headerColWidth}>
              <Button 
                onClick={handleClickOnDirection.bind(this, direction)}
              >
                Stocks {directionButtonText(direction)} <Icon>remove_circle</Icon>
              </Button>
            </GridCell>

            { isOnlyOneDirection && 
              <GridCell span="6" style={{textAlign: "right"}}>
                <Button 
                  dense
                  onClick={handleClickOnDirection.bind(this, opposite)}
                >
                  <Icon>add</Icon> Also show {directionButtonText(opposite)}
                </Button>
              </GridCell>
            }
          </Grid>
        </div>
        <GridList
            tileGutter1={false}
            headerCaption={true}
            twolineCaption={true}
            withIconAlignStart={false}
            tileAspect='3x4'
            style={{justifyContent: "center", alignItems: "center"}}
          >
          {loading && <Loader />}  
          {!loading && sortedResults && sortedResults.map( (row, i) => {
            return <Item key={i} {...row} chartKey={chartKey}/>
          })}

          {!loading && isempty(sortedResults) && 
            <Empty>No instances found. Try changing the filters.</Empty>
          }
        </GridList>
      </div>
    )
  }
}

GridDetails.defaultProps = {
  index: "MDUS",
  direction: "up"
}

export default compose(
  graphql(
    ListIndicators,
      {
        options: (ownProps) => ({
          fetchPolicy: 'cache-and-network',
          variables: { 
            id: ownProps.index,
            direction: ownProps.direction,
            projection_expression: "date_number, ticker, date_human, direction, lookahead_period, future_target_date, future_growth",
            limit: 100
          } 
        }),
        props: ({ 
          data: { 
            fetchFromTechnicalInstances = {data: [], len: 0},
            loading = true
          } 
        }) => ({
            results: [...fetchFromTechnicalInstances.data],
            len: fetchFromTechnicalInstances.len,
            LastEvaluatedKey: fetchFromTechnicalInstances.LastEvaluatedKey,
            loading: loading
        })
      }
  )
)(GridDetails);