import React, {Component} from 'react';
import { Drawer, DrawerHeader, DrawerContent} from 'rmwc/Drawer';
import { Grid, GridCell } from 'rmwc/Grid';
import SendMessage from './../../containers/SendMessage';
import {Typography} from 'rmwc/Typography';

import {EMAIL} from './../../config';
import twitter from './../../images/twitter.svg';
import logo from './../../images/white_logo_transparent.png';
import './main-menu.css';

export default class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  componentWillReceiveProps(nextProps) {
    this.setState({open: nextProps.open});
  }

  render() {
    return (
      <div dir="rtl" className="drawer-right">
        <Drawer
          temporary
          open={this.state.open}
          onClose={this.props.handleCloseDrawer}
          className="mdc-temporary-drawer--right"     
        >
          <DrawerHeader>
            <span className="drawer-right--header" style={{backgroundImage: `url(${logo})`}}>
              <span className="drawer-right--header-text">Contact Us</span>
            </span>
          </DrawerHeader>
          <DrawerContent className="drawer-right-content">
            <SendMessage />

            <div className="drawer-right--footer">
              <Grid>
                <GridCell span="2">
                  <a href="http://www.twitter.com/hivesearch.com">
                    <img src={twitter} alt="@hiveresearch" height={28} width={28}/>
                  </a>
                </GridCell>
                <GridCell span="10" style={{textAlign: 'right'}}>
                  <Typography use="caption">
                    {EMAIL}
                  </Typography>
                </GridCell>
              </Grid>
            </div>
          </DrawerContent>
        </Drawer>
      </div>
    )
  }
}