import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import isempty from 'lodash.isempty';

import Loader from './primitives/loader';
import {Icon} from 'rmwc/Icon';
import { Snackbar } from 'rmwc/Snackbar';
import './primitives/snackbar.css';
import { List, ListItem, ListItemText, ListItemSecondaryText } from 'rmwc/List';
import Empty from './primitives/empty';
import Badge from './primitives/badge';

import {directionColor, directionIcon, calcOppositeDirection} from './../utils/formatter';
import './list-overview.css';


const SnackBarMessage = (props) => {
    const oppositeDirection = calcOppositeDirection(props.direction);
    return (
      <span>
        This indicator combination has occurred <strong>{props.count}</strong> times
        when stocks proceed to go <strong>{props.direction}</strong>. Conversely, stocks have gone <strong>{oppositeDirection} {props.opposite_count}</strong> times. 
        So stocks go {props.direction} <strong>{props.ratio}X</strong> ({props.count}/{props.opposite_count}) the rate 
        that stocks go {oppositeDirection} when this indicator combination occurs.
      </span>
    )  
}

export const HumanKeyList = (props) => {
  const filtered = props.indicators && props.indicators.filter( (indicator) => {
    return indicator && !['CHF', 'CHD', 'CHU'].includes(indicator.id);
  });
  
  return (
    <span className="list-overview__list-item--indicators">
      {
        filtered.map( (indicator, i) => {
          return (
            <span key={i} className="list-overview__list-item--indicator">
                <span className="list-overview__list-item--indicator-inner">
                  {(indicator && indicator.name) || "undefined"}
                  <Icon>add_circle</Icon>
                </span>
              
            </span>
          )
        })
      }   
    </span>
  )
}

export const SecondaryLineText = (props) => {
  if (!props.ratio) return null
  const opposite = calcOppositeDirection(props.direction);
  return (
    <ListItemSecondaryText>
        {props.ratio && props.ratio > 1.15 &&
          <Badge color="green">high ratio</Badge>
        }

        {props.ratio && props.ratio < 0.85 &&
          <Badge color="red">low ratio</Badge>
        }
        {props.ratio}X of <a className="underline inherits" onClick={props.handleShowSnackBar.bind(this, <SnackBarMessage {...props} />)}>{opposite} market equivalent</a>
    </ListItemSecondaryText>
  )
}

function Item(props) {
  const color = directionColor(props.direction);
  const icon = directionIcon(props.direction);
  
  return (
    <ListItem id={props.id} ripple style={{margin: "1rem 0", borderBottom: "1px solid #f1f1f1", paddingBottom: "1rem"}}>
      <span className="list-overview__right-content">
        <Icon className="list-overview__right-icon" style={{backgroundColor: color, color: 'white', borderRadius: '50%', padding: '5px'}}>
          {icon}
        </Icon>
      </span>
      <ListItemText className="wrap list-overview__middle-content">
        <Link to={{ 
          pathname: '/details',
          search: `?index=${props.id}&direction=${props.direction}`
        }}>
          <HumanKeyList 
            indicators={props.names}
            handleFilterClick={props.handleFilterClick}
          />
        </Link>
        <SecondaryLineText {...props}/>
      </ListItemText>
      <span className="list-overview__left-content">
        <Link to={{ 
          pathname: '/details', 
          search: `?index=${props.id}&direction=${props.direction}`
        }}>
          {props.count}
        </Link>
      </span>
      
    </ListItem>
  )
}

function HeaderItem(props) {
  return (
    <ListItem className="list-overview__list-item header">
      <span className="list-overview__right-label">Nearterm<br />Growth</span>
      <span className="list-overview__middle-label">
        Technical Indicator Combinations
      </span>
      <span className="list-overview__left-label">
        Frequency
      </span>
    </ListItem>
  )
}

export default class ListOverview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snackbarIsOpen: false,
      snackbarMessage: "this is a message"
    };
    this.handleShowSnackBar = this.handleShowSnackBar.bind(this);
  }

  handleShowSnackBar(message) {
    this.setState({snackbarIsOpen: true, snackbarMessage: message})
  }

  render() {
    const {snackbarIsOpen, snackbarMessage} = this.state;
    return (
      <div>
        <div>
          <List className="list-overview__list">
            <HeaderItem />
            {this.props.loading && 
              <Loader />
            }  

            {!this.props.loading && isempty(this.props.results) && 
              <Empty>No instances found. Try changing the filters.</Empty>
            }

            {!this.props.loading && this.props.results.map( (row, i) => {
              //const names = this.add_names(row.id)
              return <Item
                handleFilterClick={this.props.handleFilterClick}
                handleShowSnackBar={this.handleShowSnackBar}
                key={i} 
                {...row}
              />
            })}
          </List>
        </div>
        <Snackbar
          show={snackbarIsOpen}
          message={snackbarMessage}
          actionText="Hide"
          actionHandler={() => this.setState({snackbarIsOpen: false})}
          timeout={60000}
        />
      </div>
    )
  }
}
