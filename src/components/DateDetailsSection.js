import React, { Component} from 'react';
import isempty from 'lodash.isempty';
import intersection from 'lodash.intersection';

import { List, ListItem } from 'rmwc/List';
import {Icon} from 'rmwc/Icon';
import {Button} from 'rmwc/Button';
import { Typography } from 'rmwc/Typography';

import Loader from './primitives/loader';
import DateDetailsItem from './DateDetailsItem';
import './DateDetailsSection.css';

import {
  directionColor, 
  calculateTickerGrowthStats,
  formatDate
} from './../utils/formatter';


function HeaderItem(props) {
  return (
    <ListItem className="list-overview__list-item header">
      <span className="list-overview__right-label">Favored <br />Direction</span>
      <span className="list-overview__middle-label">
        Technical Indicator Combinations Hit on {props.dateHuman}
      </span>
      <span className="list-overview__left-label">
        Tickers
      </span>
    </ListItem>
  )
}

function ToggleDetailButton(props) {
  const toggleText = props.showDetails ? "Hide" : "Show";
  const icon = props.showDetails ? "keyboard_arrow_up" : "keyboard_arrow_down";
  return (
    <a className="date-details--toggle-details" onClick={props.toggleDetailView.bind(this, props.direction, false)}>
      <Typography 
        use="caption"
        theme={["text-secondary-on-background"]}
      >{toggleText} Indicator Combinations <Icon>{icon}</Icon></Typography>
    </a>
  )
}

const pluralAssets = (count) => {
  return count === 1 ? "stock" : "stocks";
}

export default class DateDetailSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredTickers: []
    };
    this.handleTickerClick = this.handleTickerClick.bind(this)
  }

  componentWillUpdate(nextProps) {
    if (nextProps.dateHuman !== this.props.dateHuman) {
      this.setState({filteredTickers: []})
    }
  }

  handleTickerClick(ticker) {
    this.props.toggleDetailView(this.props.direction, true);
    const {filteredTickers} = this.state;
    let modified = []

    if (filteredTickers.includes(ticker)) {
      modified = filteredTickers.filter( t => t !== ticker);
    } else {
      modified = [...filteredTickers, ticker]
    }

    this.setState({filteredTickers: modified});
  }

  isDisabled(ticker) {
    const {filteredTickers} = this.state;
    if (isempty(filteredTickers)) {
      return false;
    } else {
      return !filteredTickers.includes(ticker);
    }
  }

  filteredItems() {
    const {filteredTickers} = this.state;
    if (isempty(filteredTickers)) {
      return this.props.items;
    } else {
      return this.props.items.filter( (item) => {
        const tickers = item.tickers.map( t => t.ticker);
        return intersection(filteredTickers, tickers).length > 0;
      });
    }    
  }

  render() {
    const direction = this.props.direction === "up" ? "increase" : "decrease";
    const buttonColor = directionColor(this.props.direction);
    const {up_count, down_count} = calculateTickerGrowthStats(this.props.tickers);
    const sortedTickers = this.props.tickers && this.props.tickers.sort( (a,b) => (b.count - a.count));
    const filteredItems = this.filteredItems()
    return (
      <div>
        <Typography use="subheading2">
          Tickers with favored price <strong>{direction}</strong> between {formatDate(this.props.dateHuman, "MM-DD")} and {formatDate(this.props.targetDate, "MM-DD")} based on indicators combinations:
        </Typography>
        <div className="date-details--tickers">
          {this.props.tickers && sortedTickers.map( (obj, i) => {
            const color = this.isDisabled(obj.ticker) ? "#ccc" : buttonColor;
            return (
              <Button 
                tag="a" 
                key={i} 
                stroked 
                dense
                style={{color: color, borderColor: color}}
                onClick={this.handleTickerClick.bind(this, obj.ticker)}
              >
                {obj.ticker} <strong>{obj.count}</strong>
              </Button>
            )
          })}
        </div>
        
        <ToggleDetailButton 
          toggleDetailView={this.props.toggleDetailView} 
          direction={this.props.direction} 
          showDetails={this.props.showDetails}
        />
        
        {this.props.showDetails &&
          <List className="list-overview__list details">
            {this.props.loading && <Loader />}
            {!this.props.loading && !isempty(this.props.items) &&
              <span>
                <HeaderItem direction={this.props.direction} dateHuman={this.props.dateHuman}/>
                { filteredItems.map( (row, i) => {
                  return <DateDetailsItem key={i} {...row}/>
                })}
              </span>
            }
          </List>
        }

        { (up_count > 0 || down_count > 0) &&
          <div className="date-details--results">
            <Typography 
              use="caption"
              theme={["text-primary-on-light"]}>
              Results:
              <strong>{up_count}</strong> {pluralAssets(up_count)} went up. <strong>{down_count}</strong> {pluralAssets(down_count)} went down.
            </Typography>
          </div>
        }
      </div>
    )
  }
}