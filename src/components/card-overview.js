import React, { Component} from 'react';

import { Card, CardPrimary, CardAction, CardActions, CardTitle, CardSubtitle} from 'rmwc/Card';

function Item(props) {
  return (
    <Card style={{width: '320px'}} className="">
      <CardPrimary>
        <CardTitle large>{props.q}</CardTitle>
        <CardSubtitle>{props.count}</CardSubtitle>
      </CardPrimary>
      <CardActions>
        <CardAction>Action 1</CardAction>
        <CardAction>Action 2</CardAction>
      </CardActions>
    </Card>
  )
}


export default class CardOverview extends Component {
  
  render() {
    const {results} = this.props;
    return (
      <div className="tableoverview--content">
        {results.map( (row, i) => {
          return <Item key={i} {...row}/>
        })}
      </div>
    )
  }
}
