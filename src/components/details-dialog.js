import React from 'react';
import {isActiveFilter, filterIndicatorsByKlass} from './../utils/filtering';

import {
  Dialog, 
  DialogBackdrop,
  DialogBody, 
  DialogFooter, 
  DialogFooterButton,
  DialogHeader,
  DialogHeaderTitle,
  DialogSurface,
} from 'rmwc/Dialog';
import {Typography} from 'rmwc/Typography';

import {FilterButton} from './primitives/buttons';
import './details-dialog.css';

const FilterSection = (props) => {
  return (
    <section className="details--section_macd">
      <Typography use="subheading2">{props.title}</Typography>
      <ul className="no-left-pad">
        {props.indicators.map( (filter, i) => {
          const isDisabled = isActiveFilter(filter, props.activeFilters, "menu");
          return (<li key={i}>
            <FilterButton 
              key={i}
              dense={true} 
              filter={filter} 
              name={filter.name} 
              handleClickOnFilter={props.handleClickOnFilter} 
              disabled={isDisabled}
            />
          </li>)
        })}
      </ul>
    </section>
  )
}

export default class DetailsDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  componentWillReceiveProps(nextProps) {
    this.setState({open: nextProps.open})
  }

  render() {
    const {allIndicators, activeFilters, handleClose} = this.props;
    
    return (
      <Dialog
        open={this.state.open}
        onClose={handleClose}
      >
        <DialogSurface>
            <DialogHeader>
              <DialogHeaderTitle>Filters</DialogHeaderTitle>
            </DialogHeader>
            <DialogBody scrollable>
              <FilterSection 
                activeFilters={activeFilters} 
                handleClickOnFilter={this.props.handleClickOnFilter}
                indicators={filterIndicatorsByKlass(allIndicators, 'MA')}
                title="Moving Averages"
              />

              <FilterSection 
                activeFilters={activeFilters} 
                handleClickOnFilter={this.props.handleClickOnFilter}
                indicators={filterIndicatorsByKlass(allIndicators, 'BB')}
                title="Bollinger Bands"
              />

              <FilterSection 
                activeFilters={activeFilters} 
                handleClickOnFilter={this.props.handleClickOnFilter}
                indicators={filterIndicatorsByKlass(allIndicators, 'MO')}
                title="Momentum"
              />

              <FilterSection 
                activeFilters={activeFilters} 
                handleClickOnFilter={this.props.handleClickOnFilter}
                indicators={filterIndicatorsByKlass(allIndicators, 'MACD')}
                title="MACD"
              />

              <FilterSection 
                activeFilters={activeFilters} 
                handleClickOnFilter={this.props.handleClickOnFilter}
                indicators={filterIndicatorsByKlass(allIndicators, 'PR')}
                title="Patterns"
              />
            </DialogBody>
            <DialogFooter>
              <DialogFooterButton cancel theme={['text-secondary-on-background']}>Close</DialogFooterButton>
            </DialogFooter>
        </DialogSurface>
        <DialogBackdrop />
      </Dialog>
    )
  }
}



