import React from 'react';
// import { TemporaryDrawer, TemporaryDrawerHeader, TemporaryDrawerContent} from 'rmwc/Drawer';
import {
  Dialog, 
  DialogBackdrop,
  DialogBody, 
  DialogFooter, 
  DialogFooterButton,
  DialogHeader,
  DialogHeaderTitle,
  DialogRoot,
  DialogSurface,
} from 'rmwc/Dialog';


export default class CountDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open) {
      this.setState({open: nextProps.open})
    }
  }

  render() {
    return (
      <Dialog
        open={this.state.open}
      >
        <DialogRoot>
          <DialogSurface>
              <DialogHeader>
                <DialogHeaderTitle>Filters</DialogHeaderTitle>
              </DialogHeader>
              <DialogBody>
                Add some filter info here.
              </DialogBody>
              <DialogFooter>
                <DialogFooterButton cancel>Cancel</DialogFooterButton>
                <DialogFooterButton accept>Filter</DialogFooterButton>
              </DialogFooter>
          </DialogSurface>
          <DialogBackdrop />
        </DialogRoot>
      </Dialog>
    )
  }
}



