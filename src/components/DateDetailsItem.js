import React, { Component} from 'react';

import { ListItem, ListItemText, ListItemSecondaryText } from 'rmwc/List';
import {Icon} from 'rmwc/Icon';

import { Link } from 'react-router-dom';
import {HumanKeyList} from './ListOverview';

import {
  directionColor, 
  directionIcon, 
  calcOppositeDirection
} from './../utils/formatter';

export const SecondaryLineText = (props) => {
  
  if (!props.ratio) return null
  const opposite = calcOppositeDirection(props.direction);
  const signularDesc = "when this indicator is hit";
  const pluralDesc = `when these ${props.numberOfIndicators} indicators are hit`
  const desc = props.numberOfIndicators > 1 ? pluralDesc : signularDesc;
  const toggledDescRemainder = props.showDescription ? `over the next 5 days ${desc}.` : <a onClick={props.toggleDescription}><strong>...</strong></a>
  
  return (
    <ListItemSecondaryText style={{whiteSpace: "normal"}}>
      Stocks have gone {props.direction} {props.ratio}X more than {opposite} {toggledDescRemainder}
    </ListItemSecondaryText>
  )
}

export default class DateDetailItem extends Component {
  constructor(props) {
    super(props);
    this.state = { showDescription: false };

    this.toggleDescription = this.toggleDescription.bind(this);
  }

  toggleDescription() {
    this.setState({showDescription: !this.state.showDescription});
  }

  render() {
    const color = directionColor(this.props.details.direction);
    const icon = directionIcon(this.props.details.direction);
    const tickers = this.props.tickers.map( obj => obj.ticker );
    return (
      <ListItem id={this.props.id} ripple style={{margin: "1rem 0", borderBottom: "1px solid #f1f1f1", paddingBottom: "1rem"}}>
        <span className="list-overview__right-content">
          <Icon className="list-overview__right-icon" style={{backgroundColor: color, color: 'white', borderRadius: '50%', padding: '5px'}}>
            {icon}
          </Icon>
        </span>
        <ListItemText className="wrap list-overview__middle-content">
          <Link to={{ 
            pathname: '/details',
            search: `?index=${this.props.id}&direction=${this.props.direction}`
          }}>
            <HumanKeyList 
              indicators={this.props.details.names}
            />
          </Link>
          <SecondaryLineText 
            {...this.props.details} 
            toggleDescription={this.toggleDescription}
            showDescription={this.state.showDescription}
            numberOfIndicators={this.props.details.names.length}
          />
        </ListItemText>
        <span className="list-overview__left-content">
            <span>{tickers.join(', ')}</span>
        </span>
      </ListItem>
    )
  }
}

