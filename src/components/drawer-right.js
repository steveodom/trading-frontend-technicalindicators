import React, { Component } from 'react';
import { TemporaryDrawer, TemporaryDrawerHeader, TemporaryDrawerContent} from 'rmwc/Drawer';
import {ListItem, ListItemText} from 'rmwc/List';


export default class DrawerRight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open) {
      this.setState({open: nextProps.open})
    }
  }

  render() {
    return (
      <TemporaryDrawer
        open={this.state.open}
        onClose={() => this.setState({open: false})}
        dir="rtl"
      >
        <TemporaryDrawerHeader dir="ltr" style={{ backgroundColor: '#f6f6f6' }}>
          TemporaryDrawerHeader
        </TemporaryDrawerHeader>
        <TemporaryDrawerContent dir="ltr" >
          <ListItem>
            <ListItemText>Cookies</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Pizza</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Icecream</ListItemText>
          </ListItem>
        </TemporaryDrawerContent>
      </TemporaryDrawer>
    )
  }
}



