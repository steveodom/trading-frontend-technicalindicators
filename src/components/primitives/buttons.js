import React from 'react';
import { Link } from 'react-router-dom';

import {Button} from 'rmwc/Button';
import { Icon  } from "rmwc/Icon";
import {directionColor, directionButtonText} from './../../utils/formatter';

export const DirectionButton = (props) => {
  let color = directionColor(props.direction);
  if (props.disabled) {
    color="#ccc";
  }
  const isDense = props.dense || false;

  return (
    <Button 
      stroked 
      style={{color: color, borderColor: color}}
      dense={isDense}
      onClick={props.handleClickOnDirectionFilter.bind(this, props.filter)}
    >
      Stocks {directionButtonText(props.direction)} 
      <Icon style={{fontSize: '0.9rem'}}>trending_{props.direction}</Icon> 
    </Button>
  )
}

export const FilterButton = (props) => {
  let color = 'rgb(51, 51, 51)';
  if (props.disabled) {
    color="#ccc";
  }
  const isDense = props.dense || false;
  return (
    <Button 
      dense={isDense} 
      stroked 
      style={{color: color, borderColor: color}}
      onClick={props.handleClickOnFilter.bind(this, props.filter)}
    >
      {props.name}
    </Button>
  )
}

export const ContactButton = (props) => {
  return (
    <Button 
      className="layout--right-action"
      dense={true} 
      stroked 
      style={{color: "#aaa", borderColor: "#aaa"}}
      onClick={this.handleOpenDrawer}
    >
      Contact Us
    </Button>
  )
}

export const DailyButton = (props) => {
  return (
    <Link to={{ pathname: '/bydate' }}>
      <Button 
        className="layout--right-action"
        dense={true} 
        stroked
        unelevated
        theme={['secondary-bg']}
        style={{borderColor: '#E91E63'}}
      >
        View Today
      </Button>
    </Link>
  )
}