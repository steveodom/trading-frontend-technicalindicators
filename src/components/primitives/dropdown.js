import React, { Component} from 'react';

import { SimpleMenu, MenuItem, MenuAnchor } from 'rmwc/Menu';
import {Button} from 'rmwc/Button';
import { Typography } from 'rmwc/Typography';
import {Icon} from 'rmwc/Icon';

export const DropdownTextWithIcon = (props) =>  {
  const icon = props.icon || "keyboard_arrow_down";
  return (
    <span>
      <Icon>{icon}</Icon> {props.text}
    </span>
  )
  
};



export default class Dropdown extends Component {  

  constructor(props) {
    super(props);
    this.state = {menuIsOpen: false};
  }
  render() {
    const {
      title, 
      optionArray,
      optionHash, 
      currentValue,
      handleDropdownChange,
      menuStyle
    } = this.props;
    
    return (
      <MenuAnchor style={menuStyle}>
        {title &&
          <Typography use="caption">{title}</Typography>
        }
        <SimpleMenu
          open={this.state.menuIsOpen}
          onClose={evt => this.setState({menuIsOpen: false})}
        >

          {optionArray && optionArray.map( (txt, i) => {
            return (
              <MenuItem key={i} onClick={handleDropdownChange.bind(this, txt)}>{txt}</MenuItem>    
            )
          })}

          {optionHash && optionHash.map( (obj, i) => {
            return (
              <MenuItem key={i} onClick={handleDropdownChange.bind(this, obj.val, obj.val2)}>{obj.label}</MenuItem>    
            )
          })}
        </SimpleMenu>

        <Button
          onClick={evt => this.setState({'menuIsOpen': !this.state.menuIsOpen})}
        >
          {currentValue}
        </Button>
      </MenuAnchor> 
    )
  }
}

Dropdown.defaultProps = {
  optionArray: [],
  currentValue: "Select:"
}