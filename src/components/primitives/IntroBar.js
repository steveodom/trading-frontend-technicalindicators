import React from 'react';
import { Typography } from 'rmwc/Typography';

import './IntroBar.css';

export default (props) => {
  
  return (
    <div className="infobar--container">
      <div className="infobar--title">
        <Typography 
          use="body1" 
          className="uppercase"
          theme={['text-secondary-on-background']}
        >
          {props.title}
        </Typography>
      </div>
      <div className="infobar--description">
        <Typography 
          use="body1"
          theme={['text-secondary-on-background']}
        >{props.children}</Typography>
      </div>
    </div>
  )
}
