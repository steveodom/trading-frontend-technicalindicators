import React from 'react';
import './badge.css'

export default (props) => {
  return (
    <span className="badge" style={{color: props.color || "green", borderColor: props.color || "green"}}>
      {props.children}
    </span>
  )
}