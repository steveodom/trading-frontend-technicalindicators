import React from 'react';
import Loader from 'react-loaders';
import "./loaders.css";

// http://jonjaques.github.io/react-loaders/
export default (props) => {
  return (
    <Loader type="ball-scale" color="#ccc"/>
  )
}