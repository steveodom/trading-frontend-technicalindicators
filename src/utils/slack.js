import request from 'request-promise';
import {APP} from './../config';

export const sendToSlack = (email, msg) =>  {
  const message = `${email} asks: ${msg} (from ${APP})`;
  const payload = {
    text: message,
    channel: "#general"
  };
  
  return request({
    method: 'POST',
    uri: 'https://hooks.slack.com/services/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',
    body: JSON.stringify(payload)
  }).then( (res) => {
    return true;
  }).catch( () => {
    return false;
  });

};
