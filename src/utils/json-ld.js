import React from 'react';

export const JSONLD_HOMEPAGE = {
  "@context": "https://schema.org",
  "@type": "Article",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "http://technical.hiveresearch.com"
  },
  "headline": "Frequency of Stock Technical Analysis Indicators by Nearterm Market Growth",
  "datePublished": "2018-02-19T08:23:54-05:00",
  "dateModified": "2018-02-19T08:25:04-05:00",
  "author": {
    "@type": "Person",
    "name": "Steve Odom"
  },
  "image": {},
  "publisher": {
    "@type": "Organization",
    "name": "Hive Research",
    "logo": {
        "@type": "ImageObject",
        "url": "http://technical.hiveresearch.com/icon.png"
    }
  },
  "mentions": [
    {
      "@type": "Intangible",
      "name": "Head & Shoulder Inverted + Closed below 200 period moving average",
      "description": "Head & Shoulder Inverted + Closed below 200 period moving average is 2.98X as likely to go down over the next 5 days than go up."
    },
    {
      "@type": "Intangible",
      "name": "Head & Shoulder + Closed above 200 period moving average",
      "description": "Head & Shoulder + Closed above 200 period moving average is 2.94X as likely to go up over the next 5 days than go down."
    },
    {
      "@type": "Intangible",
      "name": "Money Flow Index - Oversold + Low below lower Bollinger Band",
      "description": "Money Flow Index - Oversold + Low below lower Bollinger Band is 1.43X as likely to go down over the next 5 days than go up."
    },
    {
      "@type": "Intangible",
      "name": "MACD Bull Signal Cross + Closed below 5 period moving average",
      "description": "MACD Bull Signal Cross + Closed below 5 period moving average is 1.25X as likely to go down over the next 5 days than go up."
    }
  ],
  "description": "Everyday we track which technical and momentum indicators are hit (and combinations thereof) for a group of tickers. We then count them up based on whether the stock proceeds to go up or down over the next 5 days. This is the summary of those counts."
}

export const JsonLd = (data) => {
  return (<script
    type="application/ld+json"
    dangerouslySetInnerHTML={{ __html: JSON.stringify(data) }}
  />)
};
  