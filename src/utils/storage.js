import moment from 'moment';

const SHELFLIFE_IN_SECONDS = 60 * 60;

export default class Storage {
  constructor(klass = "session") {
    if (klass === "session") {
      this.storage = sessionStorage;
    } else {
      this.storage = localStorage;
    }
  }

  calcExpiresAt(s) {
    return moment().add(s, 's').format();
  }

  save(key, data) {
    this.storage.setItem(key, JSON.stringify(data));
    this.storage.setItem('expires_at', this.calcExpiresAt(SHELFLIFE_IN_SECONDS));
  }

  tokenIsFresh() {
    const expires_at = this.storage.getItem('expires_at');
    return expires_at && moment().isBefore(expires_at);
  }

  get(key) {
    if (this.tokenIsFresh()) {
      return JSON.parse(this.storage.getItem(key));
    }
  }
}
