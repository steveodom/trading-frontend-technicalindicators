import moment from 'moment';
import business from 'moment-business';

import {indicatorRelatedFilters} from './filtering';
import groupby from 'lodash.groupby';
import meanby from 'lodash.meanby';

export const getFutureDate = (currentDate, period, format="YYYY-MM-DD") =>  {
  const currentMoment = moment(currentDate, format);
  return business.addWeekDays(currentMoment, period).format(format);
};

export const formatDate = (strDate, targetFormat = "MM-DD", currentFormat = "YYYY-MM-DD") =>  {
  return moment(strDate, currentFormat).format(targetFormat)
};


export const directionButtonText = (direction) =>  {
  let text;
  switch (direction) {
    case "up":
      text = "go up";
      break;
    
    case "down":
      text = "go down";
      break;
    
    case "flat":
      text = "remain flat";
      break;
  
    default:
      text = "unknown";
      break;
  }
  return text;
};


export const directionColor = (direction) =>  {
  let color = "brown";
  if (direction === 'up') color = "green";
  if (direction === 'down') color = "red";
  return color;
};

export const directionIcon = (direction) =>  {
  let modifiedDirection = direction;
  if (direction === 'na') modifiedDirection = "na";
  return `trending_${modifiedDirection}`;
};

export const oppositeId = (id) => {
  let replace;
  let replace_with;
  let name;

  if (id.startsWith("CHD")) {
    replace = "CHD";
    replace_with = "CHU";
    name = "up";
  } else if (id.startsWith("CHU")) {
    replace = "CHU";
    replace_with = "CHD";
    name = "down"
  }
  if (replace) {
    const oid = id.replace(replace, replace_with);
    return {oid, name}
  } else {
    return false;
  }
};

export const buildTermsFromIndex = (index, indicatorNames) =>  {
  if (index) {
    const terms = []
    const ids = index.split('-');
    ids.forEach( (id) => {
      const indicator = indicatorNames.find( i => i.id === id);
      if (indicator ) {
        terms.push(indicator)
      }
    });
    return terms;
  } else {
    return {}
  }
};
 
export const buildIndexFromTerms = (terms) =>  {
  const indicators = indicatorRelatedFilters(terms);
  const ary = indicators.map( term => term.id)
  return ary.join('-')
};


export const calcOppositeDirection = (direction) =>  {
  if (direction === 'up') {
    return 'down';
  }

  if (direction === 'down') {
    return 'up';
  }

  return 'flat'; //not really used
};

export const findCountsByDirection = (historicalCounts, key, direction) =>  {
  return historicalCounts.find( item => item.id === key && item.direction === direction);
};

export const mergeCountsWithDirections = (historicalCounts, key) =>  {
  const defaultDetail = {count: 0, ratio: 0};
  let upDetail = findCountsByDirection(historicalCounts, key, "up")
  let downDetail = findCountsByDirection(historicalCounts, key, "down")
  
  let names = upDetail && upDetail.names;
  if (!names) {
    names = (downDetail && downDetail.names) || []
  }
  if (!upDetail) upDetail = defaultDetail;
  if (!downDetail) downDetail = defaultDetail;
  
 return Object.assign({}, {
    up: upDetail.count || 0, 
    down: downDetail.count || 0,
    direction: upDetail.count >  downDetail.count ? 'up' : 'down',
    ratio: upDetail.count >  downDetail.count ? upDetail.ratio : downDetail.ratio,
    names: names
  })

};

export const countDailyTickers = (details) =>  {
  const obj = {}
  details.forEach( (item) => {
    item.tickers.forEach( (t) => {    
      if (obj[t.ticker]) {
        obj[t.ticker].count += 1
      } else {
        obj[t.ticker] = {
          ticker: t.ticker, 
          count: 1,
          future_growth: t.future_growth,
          future_target_date: t.future_target_date
        }
      }
    })
  });
  return Object.values(obj);
};


export const groupAndHydrateDaily = (historicalCounts, dailyResults, currentDate) => {
  const grouped = groupby(dailyResults, 'indicators');  
  let highUpRatios = [];
  let highDownRatios = [];
  let lookAheadPeriod = 0;
  
  Object.keys(grouped).forEach( (key, i) => {
    const data = grouped[key];
    
    if (i === 0 && data.length) {
      lookAheadPeriod = data[0].lookahead_period
    }

    const obj = Object.assign({
      id: key,
      tickers: data.map( item => ({ticker: item.ticker, future_growth: item.future_growth, future_target_date: item.future_target_date })),
      details: mergeCountsWithDirections(historicalCounts, key) 
    });
    
    if (obj.details.ratio && obj.details.ratio > 1.10 ) {
      if (obj.details.direction === 'up') {
        highUpRatios.push(obj);
      }
      
      if (obj.details.direction === 'down') {
        highDownRatios.push(obj);
      }
    }
  });
  const sortedHighUpRatios = highUpRatios.sort( (a, b) => b.details.ratio - a.details.ratio);
  const sortedHighDownRatios = highDownRatios.sort( (a, b) => b.details.ratio - a.details.ratio);
  const upTickers = countDailyTickers(highUpRatios);
  const downTickers = countDailyTickers(highDownRatios);
  
  if (lookAheadPeriod < 0) {lookAheadPeriod = lookAheadPeriod * -1};

  return {
    highUpRatios: sortedHighUpRatios, 
    highDownRatios: sortedHighDownRatios,
    upTickers,
    downTickers,
    lookAheadPeriod,
    lookAheadDate: getFutureDate(currentDate, lookAheadPeriod)
  }  
};


export const calculateTickerGrowthStats = (tickers) =>  {
  const ups = tickers.filter( (ticker) => ticker.future_growth && ticker.future_growth > 0);
  const downs = tickers.filter( (ticker) => ticker.future_growth && ticker.future_growth < 0);
  return {
    up_count: ups.length,
    down_count: downs.length,
    up_mean: meanby(ups, 'future_growth'),
    down_mean: meanby(downs, 'future_growth')
  }

};
