import isempty from 'lodash.isempty';

export const upFilter = {
  id: "CHU", val: "up", condition: "eq", klass: "direction", short: "CHU"
};

export const downFilter = {
  id: "CHD", val: "down", condition: "eq", klass: "direction", short: "CHD"
};

export const flatFilter = {
  id: "CHF", val: "flat", condition: "eq", klass: "direction", short: "CHF"
};

const directionFilters = {
  "up": upFilter,
  "down": downFilter,
  "flat": flatFilter
}

export const isActiveFilter = (filter, terms, source="header") =>  {
  const active = filterByActiveTerms(filter, terms, source);
  return !isempty(active);    
};

// export const directionRelatedFilters = (terms) =>  {
//   return terms.filter( (active) => ( active.klass === "direction" ));
// };

export const indicatorRelatedFilters = (terms) =>  {
  return terms.filter( (active) => ( active.klass !== "direction" ));
};

export const isValidDetailFiltering = (terms, direction) =>  {
  return !isempty(terms) && !isempty(direction) 
};

export const filterByActiveTerms = (filter, terms, source="header") =>  {
  return terms.filter( (active) => {
    if (filter.klass === "direction") {
      return active.val === filter.val
    } else {
      return active.id === filter.id
    }    
  });
};

export const filterByNonActiveTerms = (filter, terms, source="header") =>  {
  return terms.filter( (active) => {
    if (filter.klass === "direction") {
      return active.val !== filter.val
    } else {
      return active.id !== filter.id
    }    
  });
};

export const filterIndicatorsByKlass = (indicators, klass) =>  {
  return indicators && indicators.filter((indicator) => (indicator.klass === klass));
};


export const filterHandler = (filter, terms) =>  {
  let modified = [];
  const isActive = isActiveFilter(filter, terms);
  if (isActive) {
    modified = filterByNonActiveTerms(filter, terms)
  } else {
    let obj = {}
    if (filter.klass === "direction") {
      obj = directionFilters[filter.val]
    } else {
      obj = {id: filter.id, val: 1, name: filter.name, klass: "indicator"}
    }
    modified = [...terms, obj];
  }
  return modified;
};
