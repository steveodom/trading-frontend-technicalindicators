import React from 'react';
import Home from './pages/Home';
import Details from './pages/Details';
import ByDate from './pages/ByDate';
import Datasources from './pages/Datasources';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import AWSAppSyncClient from 'aws-appsync';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import appsyncConfig from './config/AppSync';

import withTracker, {initGA} from './utils/gaTracker';

initGA('technicalindicators');

const appsyncClient = new AWSAppSyncClient({
  url: appsyncConfig.graphqlEndpoint,
  region: appsyncConfig.region,
  auth: { type: appsyncConfig.authenticationType, apiKey: appsyncConfig.apiKey }
});


const App = () => (
  <Router>
    <div>
      <Route exact path="/" component={withTracker(Home)}/>
      <Route path="/details" component={withTracker(Details)}/>
      <Route path="/bydate" component={withTracker(ByDate)}/>
      <Route path="/datasources" component={withTracker(Datasources)}/>
    </div>
  </Router>
)

const WithProvider = () => ( // 6
  <ApolloProvider client={appsyncClient}>
    <Rehydrated>
      <App />
    </Rehydrated>
  </ApolloProvider>
);

export default WithProvider;